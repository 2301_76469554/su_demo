def outer(func):
    def x(*args, **kwargs):
        print("Before function call")
        value = func(*args, **kwargs)
        print("Outer decorator")
        return value

    return x


@outer
def send_wechat(body):
    print("微信", body)
    return 100


if __name__ == '__main__':
    res = send_wechat("你好")
    print(res)
