def outer(func):
    def inner():
        # 在执行原函数前触发
        print("装饰器开始执行")
        func()  # 执行原函数
        # 在执行员函数后触发
        print("装饰器结束执行")

    return inner


@outer
def send_wechat():
    print("微信")


@outer
def send_email():
    print("邮件")


@outer
def send_sms():
    print("短信")


if __name__ == '__main__':
    send_wechat()
    send_email()
    send_sms()
