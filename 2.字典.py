# v1 = {item for item in range(10)}

# 1. 字典推导式
# v2 = {item: 100 for item in range(10)}  # {0:100,1:100.。}
# print(v2)

# v3 = {item: item + 10 for item in range(10)}  # {0:10,1:11.。}
# print(v3)


# v4 = {item: item + 10 for item in range(10) if item >5}
# print(v4) # {6: 16, 7: 17, 8: 18, 9: 19}

# names = ["wupeiqi", "root", "admin"]
# v5 = {item:100 for item in names}
# print(v5) # {'wupeiqi': 100, 'root': 100, 'admin': 100}
#
# info = [("wupeiqi", 19), ("root", 20), ("admin", 21)]
# v6 = {k: v for k, v in info if v > 19}
# print(v6)

# 2.创建字典的5种方式
# v1 = {}  # 空字典
# v2 = dict()  # 空字典
# v3 = {item: 100 for item in range(5)}
# v4 = zip(["name", "age", "email"], ["吴佩奇", "35", "wupeiqi@live.com"])
# # for item in v4:
# #     print(item)
# v4 = dict(v4)
# print(v4)
# v5 = dict.fromkeys(["name", "age", "email"], 100)
# print(v5)

# 3. value值的获取
info = {"name": "吴佩奇", "age": 35}
#
# res = info.get("name1", 100)
# print(res)  # 35

# info.setdefault("email", "wupeiqi@live.com")
# print(info)