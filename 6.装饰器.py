def outer(func):
    def x(*args, **kwargs):
        # 在执行原函数前触发
        print("装饰器开始执行")
        func(*args, **kwargs)  # 执行原函数
        # 在执行员函数后触发
        print("装饰器结束执行")

    return x


@outer  # send_wechat = outer(send_wechat)
def send_wechat(body):
    print("微信", body)


if __name__ == '__main__':
    send_wechat("你好")
