# 1. map()
# numbers = [1, 2, 3, 4, 5]
#
# res = list(map(lambda x: x ** 2, numbers))
# print(res)


# 2. filter()
# numbers = [1, 2, 3, 4, 5]
# even_numbers = filter(lambda x: x % 2 == 0, res)
# print(list(even_numbers))


# 3. reduce()
from functools import reduce

numbers = [1, 2, 3, 4, 5]

product = reduce(lambda x, y: x + y, numbers)
print(product)