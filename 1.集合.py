"""
@Time: 2024/12/30 16:51
@Author: szkingdom-11
@File: 1.集合
@Project: su_demo01
@Software: PyCharm.
"""
# 特性：容器、可变、不允许重复数据

# 1. 创建集合

# v1 = set()
# v2 = {11, 222, 33}

# 2. 添加和删除
# v2 = {11, 222, 33}
# v2.add(44)
# v2.add(55)
# print(v2)
#
# v2.discard(44)
# v2.discard(55)
# print(v2)

# 3. 交并差
# v1 = {11, 22, 33}
# v2 = {22, 33, 44}
#
# # 3.1 交集
# print(v1.intersection(v2))
# res = v1 & v2
# print(res)
#
# # 3.2 并集
# res = v1.union(v2)
# print(res)
#
# res = v1 | v2
# print(res)
#
# # 3.3 差集
# res = v1.difference(v2)
# print(res)
#
#
#
# res = v1 - v2
# print(res)
# # 3.4 对称差
# res = v2.symmetric_difference(v1)
# print(res)
# res = v1 ^ v2
# print(res)


# 4. 集合推导式

# v3 = { i for i in range(10)}
# print(v3)

data_list = ["张三", "李四", "王五"]
v3 = {"我叫" + item for item in data_list if item != "张三"}
print(v3)
